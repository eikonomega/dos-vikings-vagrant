#python_bootstrap.sh

apt-get -y install python-pip
apt-get install -y python-dev
pip install -i https://restricted.crate.io/ virtualenv

# Install MongoDB Stuff
sudo pip install mongoctl
mongoctl install-mongodb

mkdir /home/vagrant/.virtualenv

# Office Viking VirtualEnv
sudo virtualenv -v --distribute /home/vagrant/.virtualenv/office-viking
source /home/vagrant/.virtualenv/office-viking/bin/activate

pip install -i https://restricted.crate.io/ django==1.5.1
pip install -i https://restricted.crate.io/ django-tastypie
pip install -i https://restricted.crate.io/ unipath
pip install -i https://restricted.crate.io/ dj_database_url

deactivate

# Dos Vikings Analytics VirtualEnv
sudo virtualenv -v --distribute /home/vagrant/.virtualenv/dos-vikings-analytics
source /home/vagrant/.virtualenv/dos-vikings-analytics/bin/activate

pip install Flask
pip install Flask-mongoengine
pip install gunicorn

deactivate


# Dos Vikings Datastore VirtualEnv
sudo virtualenv -v --distribute /home/vagrant/.virtualenv/dos-vikings-datastore
source /home/vagrant/.virtualenv/dos-vikings-datastore/bin/activate

pip install Flask
pip install Flask-mongoengine
pip install gunicorn

deactivate
