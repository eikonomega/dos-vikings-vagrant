#!/usr/bin/python
"""
This module provides a convenient way to pull down a project repo into
the Vagrant box, create a virtualenv for it, and install its requirements
via pip.

"""

import argparse
import subprocess


def command_line_interface():
    """
    Return an configured ArgumentParser (command-line interface object).

    """
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("repo_url", help="URL of Git Repo")
    argument_parser.add_argument(
        "local_name",
        help="Name used for local repo folder and virtual env folder.")

    return argument_parser


def parse_command_line_arguments(argument_parser):
    """
    Obtain and return an the parsed command-line arguments.

    """
    return argument_parser.parse_args()


if __name__ == "__main__":
    git_command_line_parser = command_line_interface()
    command_line_arguments = parse_command_line_arguments(
        git_command_line_parser)

    # Clone the selected repo in the designated directory.
    subprocess.call(["git", "clone", command_line_arguments.repo_url,
                     "/vagrant/" + command_line_arguments.local_name])

    # Create a virtualenv for the repo.
    # Notice the reuse of local_name here for syntactical symmetry.
    subprocess.call(["sudo", "virtualenv", "--distribute",
                     "/home/vagrant/.venv/" +
                     command_line_arguments.local_name])

    # Activate the VirtualEnv
    subprocess.call(
        ["sudo", "source",
         "/home/vagrant/.venv/dos-vikings-analytics/bin/activate"]
    )

    # Install project requirements into virtualenv via the
    # project's requirements.txt file.
    subprocess.call(["sudo", "pip", "install", "-r",
                     "/vagrant/{}/requirements.txt".format(
                         command_line_arguments.local_name)])
